# vim: filetype=yaml sw=2
filename: '[% project %]-[% c("step") %]-[% c("version") %]-[% c("var/build_id") %].tar.[% c("compress_tar") %]'
git_url: https://gitlab.torproject.org/tpo/translation.git
version: '[% c("abbrev") %]'

# Linux builds still use gz as compress_tar, while other platforms use
# zst. To avoid duplicating translations tarballs which can be shared
# between platforms, we set compress_tar, except for the fenix
# translations (which are not used for Linux builds).
compress_tar: 'gz'

steps:
  base-browser:
    base-browser: '[% INCLUDE build %]'
    git_hash: 292c09a35bcda153068e77a35107baf034929067
    targets:
      nightly:
        git_hash: 'base-browser'
  tor-browser:
    tor-browser: '[% INCLUDE build %]'
    git_hash: f969264d8ca95f8d75a8dfcd61b78cb7f7e5f385
    targets:
      nightly:
        git_hash: 'tor-browser'
  mullvad-browser:
    mullvad-browser: '[% INCLUDE build %]'
    git_hash: ceaea33b0fa3ad7cf0f563392185bc9e8519079f
    targets:
      nightly:
        git_hash: 'mullvad-browser'
  fenix:
    fenix: '[% INCLUDE build %]'
    # We need to bump the commit before releasing but just pointing to a branch
    # might cause too much rebuidling of the Firefox part.
    git_hash: 16ce780d14da56fd304d2bde886848a6ca19d512
    compress_tar: 'zst'
    targets:
      nightly:
        git_hash: 'fenix-torbrowserstringsxml'
  list_updates:
    list_updates: |
      [%
        FOREACH component = [ 'base-browser', 'tor-browser', 'mullvad-browser', 'fenix' ];
          branch = pc(project, 'git_hash', { step => component, target => [ 'nightly' ] });
          commit_hash = exec('git rev-parse ' _ branch, { git_hash => branch });
          IF commit_hash == pc(project, "git_hash", { step => component });
            GET '* ' _ component _ " is up to date\n";
          ELSE;
            GET '* ' _ component _ ' can be updated: ' _ commit_hash _ "\n";
          END;
        END;
        -%]
    fetch: 1
